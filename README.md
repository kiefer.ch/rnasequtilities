
# rnaseqUtilities

Convenience wrappers to do things common in RNA-Seq analysis.

## Installation

You can install the development version of rnaseqUtilities like so:

``` r
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install(c("topGO", "ggtree"))

devtools::install_gitlab("kiefer.ch/rnasequtilities")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(rnaseqUtilities)
## basic example code
```

